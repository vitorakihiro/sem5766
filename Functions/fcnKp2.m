function r = fcnKp2( p, l )

E = 210e9;                  % M�dulo de Young [N/m]
rho = 8000;                 % Densidade do a�o comum [kg/m^3]
L = 1;                      % Comprimento da barra [m]
h = 9.55e-3;                % Altura da barra [m] 
b = 25.3e-3;                % Largura da barra [m]
A = h*b;                    % �rea da se��o transversal
I = b*h^3/12;
mi = rho*A;  

r = p^4*pi^4*E*I/(2*l^3);
end

