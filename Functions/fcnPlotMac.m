function MAC = fcnPlotMac( He, Ha, string_title )
% He is experimental mode shape, Ha is analytical mode shape
    for j = 1:size(He,2)
        for k = 1:size(Ha,2)
            MAC(j,k)= abs(He(:,j)'*Ha(:,k))^2/((Ha(:,k)'*Ha(:,k))*(He(:,j)'*He(:,j)));
        end
    end
    figure
    pcolor(MAC)
    colormap(jet)
    colorbar
    title(string_title)

end

