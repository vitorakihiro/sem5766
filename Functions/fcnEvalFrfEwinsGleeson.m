function I = fcnEvalFrfEwinsGleeson(w, A_jk, w_n, eta_jk)
    num_freq = length(w);
    num_gdl = length(w_n);
    
    I = zeros(1, num_freq);
    for freq = 1 : num_freq
        for gdl = 1 : num_gdl
            I(freq) = I(freq) ...
                + A_jk(gdl)/(1 - (w_n(gdl)/w(freq))^2*(1+1i*eta_jk(gdl)));
        end
    end
end