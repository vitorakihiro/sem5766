function r = mynewton(f,a,n)
    syms x;
    z = f(x);
    diffZ = diff(z);
    y = zeros(1,n+1); %// Pre-allocate output array 
    y(1) = a; %// First entry is the initial root

    for idx = 1 : n    
        numZ = subs(z,x,y(idx)); %// Remember to use PREVIOUS guess for next guess
        denZ = subs(diffZ,x,y(idx));
        y(idx+1) = y(idx) - double(numZ)/double(denZ); %// Place next guess in right spot  
    end
    r = y; %// Send to output
end

%from http://stackoverflow.com/questions/25129233/newton-raphson-method-in-matlab