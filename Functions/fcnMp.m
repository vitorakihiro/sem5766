function r = fcnMp( a , L)

E = 210e9;                  % M�dulo de Young [N/m]
rho = 8000;                 % Densidade do a�o comum [kg/m^3]
h = 9.55e-3;                % Altura da barra [m] 
b = 25.3e-3;                % Largura da barra [m]
A = h*b;                    % �rea da se��o transversal
I = b*h^3/12;
mi = rho*A;  

syms x;
f = (sinh(a*x)+sin(a*x))+(sin(a*L)-sinh(a*L))/(cosh(a*L)-cos(a*L))*(cosh(a*x)+cos(a*x));
r = 2*double(int(mi*x*f*f, x, [0,L]));

end

