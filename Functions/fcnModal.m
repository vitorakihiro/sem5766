% C�digo matlab desenvolvido pelo aluno Vitor Akihiro Hisano Higuti
% como parte da prova-trabalho de SEM5766 turma 2016
function [ fn, HV, phi, mr, kr, br, zeta, D ] = fcnModal( M, K, alpha, w )

n = size(M,1);
[phi, lambda] = eig(K, M);  %Autovetores e Autovalores do Sistema

B = alpha * K;

mr = phi' * M * phi;        % Matriz de Massa Modal
kr = phi' * K * phi;        % Matriz de Rigidez Modal
br = phi' * B * phi;        % Matriz de Amortecimento Modal

wn = zeros(n,1)';           % Vetor de Frequ�ncias Naturais [rad/s]
fn = zeros(n,1)';           % Vetor de Frequ�ncias Naturais [Hz]
zeta = zeros(n,1)';         % Vetor do Fator de Amortecimento

for i = 1:n
    wn(i) = sqrt(lambda(i,i));
    fn(i) = wn(i)/(2*pi);  
    zeta(i) = br(i,i) / (2*sqrt(kr(i,i) * mr(i,i)));
end

    for i = 1:n
        eval(sprintf('N{1,%i} = phi(:,%i) * phi(:,%i)'';',i,i,i));
        eval(sprintf('D{%i} = (kr(%i,%i) - mr(%i,%i) * w .^2 + sqrt(-1) * br(%i,%i) .*w);',i,i,i,i,i,i,i));
    end
    
    for i = 1: n
        for o = 1: n
            eval(sprintf('HV{%i,%i} = zeros(%i,%i);',o,i,1,size(w,2)));
            for j = 1:n
                eval(sprintf('HV{%i,%i} = HV{%i,%i} + N{1,%i}(%i,%i) ./ D{%i};',o,i,o,i,j,o,i,j))
            end            
            eval(sprintf('HV{%i,%i} = HV{%i,%i} .* w .^2;',o,i,o,i))
        end
    end

end

