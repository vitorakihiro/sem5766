function [ Ajk, eta_r ] = fcnEwinsGleeson( w_exp, frf_exp, w_n, w_response )
% This function implements Ewins-Gleeson method for modal identification
% of lightly damped structures. Vector frf_exp contains the value of FRF at
% the sampled frequencies in w_exp. Vector w_n contains the natural
% frequencies of the system. Vector w_response contains the response frequencies 
% used in the identification calculations. The elements must not coincide
% with a natural frequency. This function returns the modal constants Ajk and
% damping coefficients eta_r

    eta = 1e-8;                         % Hypothesis of low damping
    num_wn = length(w_n);
    num_response = length(w_response);
    
    R0 = zeros(num_response, num_wn);
    for j = 1:num_response
        for k = 1:num_wn
            R0(j, k) = 1 / ( 1 - (w_n(k)/w_response(j))^2 + 1i*eta );
        end
    end
    % R0 is (num_response)X(num_wn). It must be NxN to solve {Ajk} = R^{-1}{Ijk}.
    % One way is to multiply both sides of {Ijk} = R0 {Ajk} by R0':
    % R0'{Ijk} = R0'R0 {Ajk}, making it possible to solve 
    % {Ajk} = {R0'R0}^{-1}{R0'Ijk}
    
    R = R0'*R0;
    Ijk = real(interp1(w_exp, frf_exp, w_response))';
    Ijk_ = R0'*Ijk;
    Ajk = R\Ijk_;
    
    % Calculating loss factor eta_r
    eta_r = zeros(1, num_wn);
    for j = 1:num_wn
        frf_response = interp1(w_exp, frf_exp, w_n(j));
        eta_r(j) = abs(Ajk(j))/(abs(frf_response)*w_n(j)^2);
    end
end

