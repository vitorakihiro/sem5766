bSave = 0;

%% plots for report e.g p2_H11_H41.png
l = 1;
figure
    subplot(2,2,1)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(1)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{1,1}),'m','LineWidth',1.4)
    grid
    ylabel('FRF [m/s^2/N]')
    title('a) H_{11}')

    subplot(2,2,3)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(11)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{3,3}),'m','LineWidth',1.4)
    grid
    xlabel('Frequência[Hz]')
    ylabel('FRF [m/s^2/N]')
    title('b) H_{33}')

    subplot(2,2,2)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(6)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{2,2}),'m','LineWidth',1.4)
    grid
    title('c) H_{22}')

    subplot(2,2,4)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(16)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{4,4}),'m','LineWidth',1.4)
    grid
    xlabel('Frequência[Hz]')
    title('d) H_{44}')
    if(bSave), saveas(gca,'p2_EG_H11_H41.png');end

figure
    subplot(2,2,1)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(2)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{2,1}),'m','LineWidth',1.4)
    grid
    ylabel('FRF [m/s^2/N]')
    title('a) H_{21}')

    subplot(2,2,3)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(5)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{1,2}),'m','LineWidth',1.4)
    grid
    xlabel('Frequência[Hz]')
    ylabel('FRF [m/s^2/N]')
    title('b) H_{12}')

    subplot(2,2,2)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(9)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{1,3}),'m','LineWidth',1.4)
    grid
    title('c) H_{13}')

    subplot(2,2,4)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(3)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{3,1}),'m','LineWidth',1.4)
    grid
    xlabel('Frequência[Hz]')
    title('b) H_{31}')    
    if(bSave), saveas(gca,'p2_EG_H12_H42.png');end

figure
    subplot(2,2,1)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(4)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{4,1}),'m','LineWidth',1.4)
    grid
    ylabel('FRF [m/s^2/N]')
    title('a) H_{41}')

    subplot(2,2,3)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(13)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{1,4}),'m','LineWidth',1.4)
    grid
    xlabel('Frequência[Hz]')
    ylabel('FRF [m/s^2/N]')
    title('b) H_{14}')

    subplot(2,2,2)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(7)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{3,2}),'m','LineWidth',1.4)
    grid
    title('c) H_{32}')

    subplot(2,2,4)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(10)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{2,3}),'m','LineWidth',1.4)
    grid
    xlabel('Frequência[Hz]')
    title('d) H_{23}')
    if(bSave), saveas(gca,'p2_EG_H13_H43.png');end

figure
    subplot(2,2,1)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(8)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{4,2}),'m','LineWidth',1.4)
    grid
    ylabel('FRF [m/s^2/N]')
    title('a) H_{42}')

    subplot(2,2,3)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(14)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{2,4}),'m','LineWidth',1.4)
    grid
    xlabel('Frequência[Hz]')
    ylabel('FRF [m/s^2/N]')
    title('b) H_{24}')

    subplot(2,2,2)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(12)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{4,3}),'m','LineWidth',1.4)
    grid
    title('c) H_{43}')

    subplot(2,2,4)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(15)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H_EG{3,4}),'m','LineWidth',1.4)
    grid
    xlabel('Frequência[Hz]')
    title('d) H_{34}')
    
    if(bSave), saveas(gca,'p2_EG_H14_H44.png');end
