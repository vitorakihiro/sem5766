%% Ajustar FRF por FRF. Mudar valor de nFRF (entre 1 e 16)

clear all
run('t_SEM766_useData2')
close all
tic

nFRF = 16;

N = 10;
k = [1e-3; 
    4.730040744862704; 
    7.853204624095838; 
    10.995607838001671; 
    14.137165491257464;
    17.278759657399480;
    20.420352245626060;
    23.561944902040455;
    26.703537555508188;
    29.845130209103473];
for i = length(k)+1:N
    k(i) = pi*(i-0.5);
end

outputLoc = [0.031; 0.345; 0.494; 1];
inputLoc = [0.008; 0.349; 0.495; 0.97];
aPlot = 4;
bPlot = 4;
outLoc =[0.031, 0.356, 0.503, 0.98; 
    -0.005, 0.347, 0.503, 0.98;
    0.011, 0.353, 0.54, 0.98;
    0.001, 0.3465, 0.494, 1]';
inLoc = [0.008, 0.361, 0.507, 0.975;
    0.008, 0.352, 0.5004, 0.99;
    0.008, 0.352, 0.5, 0.985;
    0.008, 0.344, 0.492, 0.97;];

f = linspace(0,1000,1600);
w = 2*pi*f;  
L = 1;
mp = eye(N);
kp = eye(N);
recalc = 0;
if exist ('kp.mat','file'), load('kp'); else recalc = 1; end
if exist ('mp.mat','file'), load('mp'); else recalc = 1; end

for i = 1:length(mp)
   if(recalc), mp(i,i) = fcnMp(k(i),L);end
   if(recalc), kp(i,i) = fcnKp(k(i),L);end
   fn(i)= sqrt(kp(i,i)/mp(i,i))/2/pi;
end
    
alpha = 1e-7;
cp = alpha*kp;
figure(1)
l =1;
H = cell(4,4);
for b = 1:4
    for a = 1:4
        sum = 0;
        sum0 = 0;
        for p = 1:length(kp)
            den = (kp(p,p)-mp(p,p)*w.^2 + sqrt(-1)*cp(p,p)*w);
            sum0 = sum0 + fcnPthModalShape( k(p), L, outputLoc(a) )...
                *fcnPthModalShape( k(p), L, inputLoc(b))./den;
            sum = sum + fcnPthModalShape( k(p), L, outLoc(l) )...
                *fcnPthModalShape( k(p), L, inLoc(l))./den;
        end
        s = sprintf('a: %i b: %i outLoc: %3.3f inLoc: %3.3f',a,b,outLoc(l),inLoc(l));
        H{a,b} = -w.^2.*sum;
        H0{a,b} = -w.^2.*sum0;
               
    figure(1)
        subplot(4,4,l)
        semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(l)}(:,2)),'LineWidth',2)
        hold on
        semilogy(f,abs(H{a,b}),'r')
        figure(2)
        subplot(4,4,l)
        plot(freqS,imag(H1_2_cell_shaker{1,k_shaker(l)}(:,2))/...
            max(abs(imag(H1_2_cell_shaker{1,k_shaker(l)}(:,2)))),'LineWidth',2)
        hold on
        plot(f,imag(H{a,b})/max(abs(imag(H{a,b}))),'ro-')
        if(l == nFRF)
            figure(3)            
            semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(l)}(:,2)),'LineWidth',2)
            hold on
            semilogy(f,abs(H{a,b}),'r')
            disp(s)
        end
        l = l + 1;
    end
end
figure(3)
