%% Compara os experimentos de shaker com martelo de impacto

clear all
run('t_SEM766_useData2')
close all
addpath('Functions')

freqS = H1_2_cell_shaker{1,1}(:,1);
freqH = H1_2_cell_hammer{1,1}(:,1);

l = 1;
for i = 21:31
    figure(1)
   subplot(3,4,l)
   semilogy(freqH, abs(H1_2_cell_hammer{1,i}(:,2)))
   ylim([1e-2,1e2])
   
   figure(2)
   subplot(3,4,l)
   plot(freqH,C1_2_cell_hammer{1,i}(:,2));
   C(l) = sum(C1_2_cell_hammer{1,i}(:,2));
   
   figure(3)
   semilogy(freqH, abs(H1_2_cell_hammer{1,i}(:,2)))
   hold on
   
   figure(4)
   subplot(3,4,l)
    plot(freqH,imag(H1_2_cell_hammer{1,i}(:,2))/...
        max(abs(imag(H1_2_cell_hammer{1,i}(:,2)))),'LineWidth',2)
   
   l = l + 1;
    
end

n  = 1;
figure
semilogy(freqS, abs(H1_2_cell_shaker{1,k_shaker(n)}(:,2)),'b.-')
hold on 
semilogy(freqH, abs(H1_2_cell_hammer{1,k_hammer(n)}(:,2)),'r.-')
grid 
legend('shaker','hammer','Location','NorthEast')
xlabel('Frequência [Hz]')
ylabel('FRF [m/s^2/N]')
