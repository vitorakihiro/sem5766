clear all
close all
clc

if ispc, fSlash = '\';elseif isunix, fSlash='//'; end

DATA_SET = 1;
switch(DATA_SET)
    case 1
        experiment = 'shaker';
        k = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17];
    case 2
        experiment = 'hammer';
        k = [4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
    case 3
        experiment = 'transm';
        k = [6,7,8,10,11,12,13,15];        
end
load(sprintf('Data%s%sSEM766_2016_%s.mat',fSlash,fSlash,experiment));
eval(sprintf('H1_2_cell = H1_2_cell_%s;',experiment));
eval(sprintf('C1_2_cell = C1_2_cell_%s;',experiment));

