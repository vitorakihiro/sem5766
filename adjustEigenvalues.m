clc
addpath('Functions')

N = 10;
k = [1e-3; 
    4.730040744862704; 
    7.853204624095838; 
    10.995607838001671; 
    14.137165491257464;
    17.278759657399480;
    20.420352245626060;
    23.561944902040455;
    26.703537555508188;
    29.845130209103473];
for i = length(k)+1:N
    k(i) = pi*(i-0.5);
end

f = @(x)cos(x)*cosh(x)-1;

n_iteracoes = 10;
for i = 1:length(k)
    r(:,i) = mynewton(f,k(i),n_iteracoes);
end

%% Check if new value improves cos(k).*cosh(k)=1

cos(k).*cosh(k)-1