% Codigo matlab desenvolvido pelo aluno Vitor Akihiro Hisano Higuti
% como parte da prova-trabalho de SEM5766 turma 2016

clear all
close all
clc
experiment = 'transm';
k = [6,7,8,10,11,12,13,15];
 
if ispc, fSlash = '\';elseif isunix, fSlash='//'; end
load(sprintf('Data%s%sSEM766_2016_%s.mat',fSlash,fSlash,experiment));
eval(sprintf('H1_2_cell = H1_2_cell_%s;',experiment));
eval(sprintf('H1_3_cell = H1_3_cell_%s;',experiment));

descriptionH12 = {'A1/F1', 'A1/F1', 'A1/F1', 'A2/A1','A3/A1', 'A4/A1', 'A2/A4','A3/A4'};
descriptionH13 = {'A2/F1', 'A3/F1', 'A4/F1', '-','-', '-', '-','-'};
% Frequency interval is the same for all data
freq = H1_2_cell{1,6}(:,1);
for i = 1: length(k)
    H{i} = H1_2_cell{1,k(i)}(:,2);
    H13{i} = H1_3_cell{1,k(i)}(:,2);
    figure(1)
    subplot(4,2,i)        
    semilogy(freq,abs(H{i}))
    t = sprintf('%i: RUN #%i: %s',i, k(i), char(descriptionH12(i)));
    title(t)
    figure(2)
    subplot(4,2,i)        
    semilogy(freq,abs(H13{i}))
    t = sprintf('RUN #%i: %s',k(i), char(descriptionH13(i)));
    title(t)
end
fig = figure(1);
set(fig,'Name','Entrada: Canal 1 Sa�da: Canal 2','NumberTitle','off');
fig = figure(2);
set(fig,'Name','Entrada: Canal 1 Sa�da: Canal 3','NumberTitle','off');
%%
rhoAl = 2697; % alum�nio [kg/m^3]
rhoSS = 8100; % a�o inox [kg/m^3]
ESS = 180*1e9; %N/m^2

L1 = 80.4e-3;
L2 = 80.3e-3;

b = 25.3e-3;
h = 1.25e-3;

m_viga1 = ((L1 * b * h) * rhoSS);
m_viga2 = ((L2 * b * h) * rhoSS);
m = ((12.64*80 + 12.57*80 - (12.57-7.51)*70)*25.3*rhoAl + 1.25*25.3*25.3*rhoSS)*1e-9;
m1 = ((9.6*9.48*45*rhoSS)*2 + (25.3*1.25*9.6*rhoSS))*1e-9;
m2 = m1;
m_eq1 = m1 + 33/140*m_viga1;
m_eq2 = m2 + 33/140*m_viga2;
I = b*h^3/12;
C = 1.1;
k1 = C*ESS*I/L1^3;
k2 = C*ESS*I/L2^3;   

M3 = [m_eq1 0 0 ; ...               % Matriz de Massa
     0  m 0 ;
     0 0 m_eq2];
K3 = [k1 -k1 0;
      -k1 k1+k2 -k2; ...            % Matriz de Rigidez
      0   -k2  k2];
alpha3 = 10^(-4);                   % Amortecimento Proporcional

f = linspace(0,500,5000);           % [Hz]
w = 2*pi * f;                       % [rad/s]
 
[fn3,HV3, phi3, mr3, kr3, br3, zeta3, D3] = fcnModal(M3,K3,alpha3,w);

numX1 = [k1*m1 0 k1*k2];
numX2 = [m1^2 (k1+k2)*m1 k1*k2];
numX3 = [k2*m1 0 k1*k2];
den = [m1^2*m 0 (k1*m1^2+k2*m1^2+m*m1*k2+m*m1*k1) 0 k1*k2*(2*m1+m) 0 0];

numT1 = k1;
denT1 = [m1 0 k1];

numT2 = k2;
denT2 = [m2 0 k2];

H12 = tf(numX1,den);
H22 = tf(numX2,den);
H32 = tf(numX3,den);

F1 = tf(numT1,denT1);
F2 = tf(numT2,denT2);

[mag1,phase1] = bode(H12,w);
[mag2,phase2] = bode(F1,w);
[mag2a,phase2a] = bode(F2,w);
for i = 1:length(mag1)
    mag(i) = mag1(:,:,i);
    mag_2(i) = mag2(:,:,i);
    mag_2a(i) = mag2a(:,:,i);
end
mag = mag.*w.^2;

fig=figure(4);
set(fig,'Position',[50 50 900 600],'Name','Modelo 3GDL','NumberTitle','off')
subplot(2,2,1)
semilogy(freq,abs(H1_3_cell{1,k(2)}(:,2)))
hold on
semilogy(f,0.5*abs(HV3{1,2}))
legend('experimental','calculado','Location','Best')
t = sprintf('RUN #%i: %s comparado � FRF H_{12}',k(2), char(descriptionH13(2)));
title(t)

subplot(2,2,2)
semilogy(freq,abs(H1_2_cell{1,k(1)}(:,2)))
hold on
semilogy(f,0.35*abs(HV3{2,2}))
% semilogy(f,abs(HV3{2,2}))
legend('experimental','calculado','Location','Best')
t = sprintf('RUN #%i: %s comparado � FRF H_{22}',k(1), char(descriptionH12(1)));
title(t)

subplot(2,2,3)
semilogy(freq,abs(H1_2_cell{1,k(5)}(:,2)))
hold on
semilogy(f,mag_2)
t = sprintf('RUN #%i: %s comparado � transmissibilidade A_1',k(5), char(descriptionH12(5)));
title(t)
legend('experimental','calculado','Location','Best')

subplot(2,2,4)
semilogy(freq,abs(H1_2_cell{1,k(4)}(:,2)))
hold on
semilogy(f,mag_2a)
t = sprintf('RUN #%i: %s comparado � transmissibilidade A_2',k(4), char(descriptionH12(4)));
title(t)
legend('experimental','calculado','Location','Best')


%% MODELO DE 5GDL
Nx = 2;                                 % N�mero de divis�es da viga

I = b*h^3/12;
C = 1.36;
alpha = 2*10^-6;
keq1 = Nx*C * ESS * I/(L1^3);
keq2 = Nx*C * ESS * I/(L2^3);

C2 = 0.17;

M5 = [m1 + C2*m_viga1/Nx 0 0 0 0;
    0 C2*m_viga1/Nx 0 0 0;
    0 0 m 0 0;
    0 0 0 C2*m_viga2/Nx 0;
    0 0 0 0 m2 + C2*m_viga2/Nx];

K5 = [keq1 -keq1 0 0 0;
    -keq1 keq1+keq1 -keq1 0 0;
    0 -keq1 keq1+keq2 -keq2 0;
    0 0 -keq2 keq2+keq2 -keq2;
    0 0 0 -keq2 keq2];
    
[fn5, HV5, phi5, mr5, kr5, br5, zeta5] = fcnModal(M5,K5,alpha,w);

k5_1 = K5(1,1);
k5_2 = -K5(1,2);
k5_3 = -K5(2,3);
k5_4 = K5(5,5);

m5_1 = M5(1,1);
m5_2 = M5(2,2);
m5_4 = M5(4,4);
m5_5 = M5(5,5);

num5T1 = k5_1*k5_2;
den5T1 = [m5_2*m5_1 0 k5_1*(m5_1+m5_2)+k5_2*m5_1 0 k5_1*k5_2];

num5T2 = -k5_3*k5_4;
den5T2 = [m5_5*m5_4 0 k5_4*(m5_5+m5_4)+k5_4*m5_5 0 k5_3*k5_4];

F5_13 = tf(num5T1,den5T1);
F5_53 = tf(num5T2,den5T2);

[mag3, phase3] = bode(F5_13,w);
[mag4, phase4] = bode(F5_53,w);
for i = 1:length(mag3)
    mag5_13(i) = mag3(:,:,i); 
    mag5_53(i) = mag4(:,:,i); 
end

fig = figure(6);
set(fig,'Position',[200 30 900 600],'Name','Modelo 5GDL','NumberTitle','off')
subplot(2,2,1)
semilogy(freq,abs(H1_3_cell{1,7}(:,2)))
hold on
semilogy(f,0.5*abs(HV5{3,1}))
t = sprintf('RUN #%i: %s comparado � FRF H_{12}',k(2), char(descriptionH13(2)));
title(t)
legend('experimental','calculado','Location','Best')

subplot(2,2,2)
semilogy(freq,abs(H1_2_cell{1,k(1)}(:,2)))
hold on
semilogy(f,0.35*abs(HV5{3,3}))
t = sprintf('RUN #%i: %s  comparado � FRF H_{22}',k(1), char(descriptionH12(1)));
title(t)
legend('experimental','calculado','Location','Best')

subplot(2,2,3)
semilogy(freq,abs(H1_2_cell{1,k(5)}(:,2)))
hold on
semilogy(f,mag5_13)
t = sprintf('RUN #%i: %s comparado � transmissibilidade A_1',k(5), char(descriptionH12(5)));
title(t)
legend('experimental','calculado','Location','Best')

subplot(2,2,4)
semilogy(freq,abs(H1_2_cell{1,k(4)}(:,2)))
hold on
semilogy(f,mag5_53)
t = sprintf('RUN #%i: %s comparado � transmissibilidade A_2',k(4), char(descriptionH12(4)));
title(t)
legend('experimental','calculado','Location','Best')