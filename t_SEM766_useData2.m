% clear all
DATA_SET = 1;
clearvars -except numH DATA_SET
close all
clc

if ispc, fSlash = '\';elseif isunix, fSlash='//'; end

        k_shaker = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17];
        k_hammer = [4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
        switch(DATA_SET)
            case 1
                k = k_shaker;
                experiment = 'shaker';
            case 2
                k = k_hammer;
                experiment = 'hammer';
        end
        
load(sprintf('Data%s%sSEM766_2016_%s.mat',fSlash,fSlash,'shaker'));
load(sprintf('Data%s%sSEM766_2016_%s.mat',fSlash,fSlash,'hammer'));
for i = 1:34
   H1_2_cell_hammer{1,i}(:,2) = 9.81*H1_2_cell_hammer{1,i}(:,2) ;
end
if(DATA_SET == 1 || DATA_SET == 2)
    eval(sprintf('H1_2_cell = H1_2_cell_%s;',experiment));
%     eval(sprintf('C1_2_cell = C1_2_cell_%s;',experiment));
    %Frequency interval is the same for all data
    freqS = H1_2_cell_shaker{1,1}(:,1);
    freqH = H1_2_cell_hammer{1,1}(:,1);

    %H{i,j} where i is the output and j is the input
    
    % H_{ij} = U_i / F_j
    
    %H{i,j} i: row and j: column
    off_x = 100;
    off_y = 0;
% % % % %     for  i = 1:3
% % % % %        fig(i) = figure(i);
% % % %        set(fig(i),'position',[off_x, off_y, 1000, 600]);
% % % % %        off_x = off_x + 50;
% % % % %        off_y = off_y + 25;
% % % % %     end
    l = 1;
    fig = figure(1);
   set(fig,'position',[off_x, off_y, 1100, 650]);
    
    
    for j = 1: 4
        for i = 1:4
            H{i,j} = H1_2_cell_shaker{1,k_shaker(l)}(:,2);
%             C{i,j} = C1_2_cell{1,k(l)}(:,2);
            figure(1)
            subplot(4, 4, l )
            semilogy(freqS,abs(H{i,j}),'LineWidth',2)
            figure(2)
            subplot(4, 4, l )
            plot(freqS,imag(H{i,j}/max(abs(imag(H{i,j})))),'LineWidth',2)
            
%             hold on
%             semilogy(freqH,abs(H1_2_cell_hammer{1,k_hammer(l)}(:,2)))
            
%             t = sprintf('i:%i o:%i data set #%i',j,i,k(l));
%             title(t);

%             figure(2)
%             subplot(4, 4, l )
%             hold on
%             plot(freq,(C{i,j}))
%             t = sprintf('i:%i o:%i data set #%i',j,i,k(l));
%             title(t);

            l = l + 1;
        end
    end
%     l =1;
%     for j = 1:4
%         for i = 1:4
%             diff{i,j} = H{i,j} - H{j,i};
%             diffSum(i,j) = sum(sum((H{i,j} - H{j,i}).^2));
%             diffStdDev(i,j) = std(H{i,j} - H{j,i});
%             diffMax(i,j) = max(abs(H{i,j} - H{j,i}));
%             figure(3)
%             subplot(4, 4, l )
%             plot(freq,(diff{i,j}))
%             t = sprintf('i:%i o:%i data set #%i',j,i,k(l));
%             title(t);
% 
%             l = l + 1;
%         end
%     end
% % % %     fig= figure(4);
% % % %     set(fig,'Position',[100 100 1200 500])
% % % %     subplot(1,2,1)
% % % %     semilogy(freq, abs(H1_2_cell{1,k(numH)}(:,2)) )
% % % %     hold on
% % % %     semilogy(freq, abs(H1_2_cell{1,k(numH)}(:,2)),'r.')

else    
    eval(sprintf('H1_2_cell = H1_2_cell_%s;',experiment));
    eval(sprintf('H1_3_cell = H1_3_cell_%s;',experiment));

    descriptionH12 = {'A1/F1', 'A1/F1', 'A1/F1', 'A2/A1','A3/A1', 'A4/A1', 'A2/A4','A3/A4'};
    descriptionH13 = {'A2/F1', 'A3/F1', 'A4/F1', '-','-', '-', '-','-'};
    % Frequency interval is the same for all data
    freq = H1_2_cell{1,6}(:,1);
    for i = 1: length(k)
        H{i} = H1_2_cell{1,k(i)}(:,2);
        H13{i} = H1_3_cell{1,k(i)}(:,2);
        figure(1)
        subplot(4,2,i)        
        semilogy(freq,abs(H{i}))
        t = sprintf('%i: RUN #%i: %s',i, k(i), char(descriptionH12(i)));
        title(t)
        figure(2)
        subplot(4,2,i)        
        semilogy(freq,abs(H13{i}))
        t = sprintf('RUN #%i: %s',k(i), char(descriptionH13(i)));
        title(t)
    end
%     figure(3)
%     subplot(2,2,1)
%     H1{i} = H1_2_cell{1,9}(:,2);
% %     plot(freq,20*log10(H1{i}))
%     semilogy(freq,abs(H1{i}))
%     subplot(2,2,2)
%     H2{i} = H1_2_cell{1,10}(:,2);
%     plot(freq,20*log10(H2{i}))
%     subplot(2,2,3)
%     H1{i} = H1_2_cell{1,14}(:,2);
%     plot(freq,20*log10(H1{i}))
%     subplot(2,2,4)
%     H2{i} = H1_2_cell{1,15}(:,2);
%     plot(freq,20*log10(H2{i}))
end