clear all
clc
DATA_SET = 3;

if ispc, fSlash = '\';elseif isunix, fSlash='//'; end

switch(DATA_SET)
    case 1
        experiment = 'shaker';
        nFolder = 20;
        firstFolder  = 1;
    case 2
        experiment = 'hammer';
        nFolder = 34;
        firstFolder  = 1;
    case 3
        experiment = 'transm'; %transmissibility
        nFolder = 15;
        firstFolder  = 6;
end 
if exist(sprintf('Data%s%sSEM766_2016_%s.mat',fSlash,fSlash,experiment),'file'), 
    error('%s already exists!!!', sprintf('SEM766_2016_%s.mat',experiment));
end

for folderNum = firstFolder: nFolder
    folder = strcat(pwd,sprintf('%sData%s%ssem766_%s%sMATLAB%5.5d%s', fSlash, ...
        fSlash, fSlash, experiment, fSlash, folderNum, fSlash));
    addpath(folder);
    fileList = dir(strcat(sprintf('%s*.mat',folder)));
    filesBefore = whos;
    load(fileList.name)
    filesAfter = whos;
    for i = 1: size(filesAfter,1)
        saveVar = true;
        for j = 1: size(filesBefore,1)
            if(strcmp(filesAfter(i).name, filesBefore(j).name) ...
                    || strcmp(filesAfter(i).name, 'filesBefore') )
                saveVar  = false;
                break
            end
        end
         if saveVar
             eval(sprintf('%s_cell_%s{%d}=%s;',filesAfter(i).name, experiment,folderNum, filesAfter(i).name))
         end            
         if(~strcmp(filesAfter(i).name, 'filesBefore')&&~strcmp(filesAfter(i).name, 'filesAfter')&& isempty(strfind(filesAfter(i).name, '_cell'))  )
             filesAfter(i).name
            if ~strcmp(filesAfter(i).name,'fSlash') && ~strcmp(filesAfter(i).name,'experiment')                    
                clearvars(filesAfter(i).name)
            end
         end
    end
end
 
filesAfter = whos;
    for i = 1: size(filesAfter,1)
        saveVar = true;
        for j = 1: size(filesBefore,1)
            if(isempty(strfind(filesAfter(i).name, '_cell') ))
                saveVar  = false;
                break
            end
        end
        if saveVar
            if ~exist(sprintf('Data%s%sSEM766_2016_%s.mat',fSlash,fSlash,experiment),'file')
                save(sprintf('Data%s%sSEM766_2016_%s.mat',fSlash,fSlash,experiment),filesAfter(i).name)
            else
                save(sprintf('Data%s%sSEM766_2016_%s.mat',fSlash,fSlash,experiment),filesAfter(i).name,'-append')
            end
        end       
    end