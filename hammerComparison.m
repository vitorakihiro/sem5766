%% Compara as medicoes realizadas pelos alunos

clear all
run('t_SEM766_useData2')
close all
addpath('Functions')

freq = H1_2_cell_shaker{1,1}(:,1);

l = 1;
for i = 21:31
    figure(1)
    subplot(3,4,l)
    semilogy(freq, abs(H1_2_cell_hammer{1,i}(:,2)))
    ylim([1e-2,1e2])

    figure(2)
    subplot(3,4,l)
    plot(freq,C1_2_cell_hammer{1,i}(:,2));
    C(l) = sum(C1_2_cell_hammer{1,i}(:,2));

    figure(3)
    semilogy(freq, abs(H1_2_cell_hammer{1,i}(:,2)))
    hold on

    figure(4)
    subplot(3,4,l)
    plot(freq,imag(H1_2_cell_hammer{1,i}(:,2))/...
        max(abs(imag(H1_2_cell_hammer{1,i}(:,2)))),'LineWidth',2)
    hold on

    figure(5)
    subplot(3,4,l)   
    plotyy(freq,C1_2_cell_hammer{1,i}(:,2), ...
       freq, abs(H1_2_cell_hammer{1,i}(:,2)), ...
       'plot','semilogy')   
   l = l + 1;    
end

figure(3)
grid
xlabel('Frequência [Hz]')
ylabel('FRF H_{11} [m/s^2/N]')

figure(6)
   [ax,l1,l2] = plotyy(freq,C1_2_cell_hammer{1,22}(:,2), ...
       freq, abs(H1_2_cell_hammer{1,22}(:,2)), ...
       'plot','semilogy');
   set(l1,'Color','b','LineWidth',1.3)
   set(l2,'Color','r','LineWidth',1.3)

xlabel('Frequência [Hz]')
ylabel(ax(1),'Coerência')
ylabel(ax(2),'FRF H_{11} [m/s^2/N]');

figure(7)
   [ax,l1,l2] = plotyy(freq,C1_2_cell_hammer{1,31}(:,2), ...
       freq, abs(H1_2_cell_hammer{1,31}(:,2)), ...
       'plot','semilogy');
   set(l1,'Color','b','LineWidth',1.3)
   set(l2,'Color','r','LineWidth',1.3)

xlabel('Frequência [Hz]')
ylabel(ax(1),'Coerência')
ylabel(ax(2),'FRF H_{11} [m/s^2/N]');