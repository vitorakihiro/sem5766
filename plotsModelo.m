%% rodar projecto2v2.m primeiro
% plota H: FRFs modeladas
% depois pode dar load('HmodeloV1') para obter H na versao original do modelo,
% sem nenhuma alteracao
%
% ou load('HmodeloV2') para E = 191GPa, mas sem alterar os pontos de entrada
% e de saida
%
% project2v2.m tem o modelo final

% mudar bSave para salvar os plots

bSave = 0;
%% plots for report e.g p2_H11sXmV1.png
figure
i =1;j=1;l =1;
% i =3;j=4;l =15;
semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(l)}(:,2)),'b.-','LineWidth',2)
hold on
semilogy(f_vec,abs(H{i,j}),'r.-')
grid
xlabel('Frequência [Hz]')
ylabel('FRF [m/s^2/N]')
legend('shaker','modelo')

%% plots for report e.g p2_H11_H41.png
l = 1;
figure
    subplot(2,2,1)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(1)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{1,1}),'r','LineWidth',1.4)
    grid
    ylabel('FRF [m/s^2/N]')
    title('(a) H_{11}')

    subplot(2,2,3)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(11)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{3,3}),'r','LineWidth',1.4)
    grid
    xlabel('Frequência [Hz]')
    ylabel('FRF [m/s^2/N]')
    title('(b) H_{33}')

    subplot(2,2,2)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(6)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{2,2}),'r','LineWidth',1.4)
    grid
    title('(c) H_{22}')

    subplot(2,2,4)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(16)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{4,4}),'r','LineWidth',1.4)
    grid
    xlabel('Frequência [Hz]')
    title('(d) H_{44}')
    if(bSave), saveas(gca,'p2_H11_H44.png');end

figure
    subplot(2,2,1)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(2)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{2,1}),'r','LineWidth',1.4)
    grid
    ylabel('FRF [m/s^2/N]')
    title('(a) H_{21}')

    subplot(2,2,3)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(5)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{1,2}),'r','LineWidth',1.4)
    grid
    xlabel('Frequência [Hz]')
    ylabel('FRF [m/s^2/N]')
    title('(b) H_{12}')

    subplot(2,2,2)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(9)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{1,3}),'r','LineWidth',1.4)
    grid
    title('(g) H_{13}')

    subplot(2,2,4)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(3)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{3,1}),'r','LineWidth',1.4)
    grid
    xlabel('Frequência [Hz]')
    title('(h) H_{31}')    
    if(bSave), saveas(gca,'p2_H12_H13.png');end

figure
    subplot(2,2,1)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(4)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{4,1}),'r','LineWidth',1.4)
    grid
    ylabel('FRF [m/s^2/N]')
    title('(c) H_{41}')

    subplot(2,2,3)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(13)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{1,4}),'r','LineWidth',1.4)
    grid
    xlabel('Frequência [Hz]')
    ylabel('FRF [m/s^2/N]')
    title('(d) H_{14}')

    subplot(2,2,2)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(7)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{3,2}),'r','LineWidth',1.4)
    grid
    title('(i) H_{32}')

    subplot(2,2,4)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(10)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{2,3}),'r','LineWidth',1.4)
    grid
    xlabel('Frequência [Hz]')
    title('(j) H_{23}')
    if(bSave), saveas(gca,'p2_H14_H23.png');end

figure
    subplot(2,2,1)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(8)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{4,2}),'r','LineWidth',1.4)
    grid
    ylabel('FRF [m/s^2/N]')
    title('(e) H_{42}')

    subplot(2,2,3)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(14)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{2,4}),'r','LineWidth',1.4)
    grid
    xlabel('Frequência [Hz]')
    ylabel('FRF [m/s^2/N]')
    title('(f) H_{24}')

    subplot(2,2,2)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(12)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{4,3}),'r','LineWidth',1.4)
    grid
    title('(k) H_{43}')

    subplot(2,2,4)
    semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(15)}(:,2)),'b.-')
    hold on
    semilogy(f_vec,abs(H{3,4}),'r','LineWidth',1.4)
    grid
    xlabel('Frequência [Hz]')
    title('(l) H_{34}')
    
    if(bSave), saveas(gca,'p2_H24_H34.png');end
