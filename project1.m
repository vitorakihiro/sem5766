clear all
close all
clc
 
M1 = 10;
M2 = 1.5;
 
B1 = 10;
B2 = 4;
 
k1 = 5*10^5;
k2 = 10^5;
 
N = 2;                      %N�mero de graus de liberdade do Sistema
 
% Os sistemas a e b do projeto apresentam mesmas matrizes de Massa, Rigidez
% e Amortecimento, diferenciando-se apenas nas suas entradas. C�lculo da
% Matriz H(w) n�o depende das entradas do sistema (aparentemente)
m = [M1 0; ...              %Matriz de Massa
     0  M2];
  
k = [k1+k2 -k2; ...         %Matriz de Rigidez
     -k2   k2];
  
 b = [B1+B2 -B2; ...       %Matriz de Amortecimento (N�o Proporcional)
      -B2   B2];           % n�o usada  
 
alpha = 10^(-5);            %Matriz de Amortecimento Proporcional
bp = alpha * k;
 
[phi, lambda] = eig(k, m);  %Autovetores e Autovalores do Sistema
 
phi_1 = phi(:,1);           % Autovetor do Modo 1
phi_2 = phi(:,2);           % Autovetor do Modo 2
 
mr = phi' * m * phi;        %Matriz de Massa Modal
br = phi' * bp * phi;       %Matriz de Amortecimento Modal -> Diagonais
kr = phi' * k * phi;        %Matriz de Rigidez Modal
 
wn = zeros(N,1)';
fn = zeros(N,1)';
mrd = zeros(N,1)';
krd = zeros(N,1)';
brd = zeros(N,1)';
 
for i = 1:N
    wn(i) = sqrt(lambda(i,i));  %Vetor de frequ�ncias naturais (rad/s)
    fn(i) = wn(i)/(2*pi);       %Vetor de frequ�ncias naturais (Hz)
    mrd(i) = mr(i,i);           %Vetor de Massa Modal
    krd(i) = kr(i,i);           %Vetor de Rigidez Modal
    brd(i) = br(i,i);           %Vetor de Amorteciment Modal
    wna(i) = sqrt(krd(i)/mrd(i));
end
 
% fn_max � 47Hz: intervalo de frequ�ncias estudado ser� de 60Hz
f = linspace(0,60,5000);        % em Hz
w = 2*pi * f;                   % em rad/s
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parte A
zeta = brd ./ (2*sqrt(krd .* mrd)); %Vetor do fator de amortecimento
 
i = sqrt(-1);
D1 = mrd(1) *(wn(1)^2 - w .^2 + i* 2* zeta(1)* wn(1) .*w);
D2 = mrd(2) *(wn(2)^2 - w .^2 + i* 2* zeta(2)* wn(2) .*w);
 
D1a = kr(1,1) - mr(1,1)*w.^2+1i*br(1,1)*w;
D2a = kr(2,2) - mr(2,2)*w.^2+1i*br(2,2)*w;
%%%%%%%%%% Usando f�rmulas que o Varoto usou em aula
 
    N1 = phi_1 * phi_1';
    N2 = phi_2 * phi_2';
 
    H11V = N1(1,1) ./D1 + N2(1,1)./D2;
    H22V = N1(2,2) ./D1 + N2(2,2)./D2;
    H12V = N1(1,2) ./D1 + N2(1,2)./D2;
    H21V = H12V;
 
%%%%%%%%%% Usando f�rmulas dos Slides
    H11 = (phi(1,1) * phi(1,1)) ./D1 + (phi(1,2) * phi(1,2)) ./D2;
 
    H22 = (phi(2,1) * phi(2,1)) ./D1 + (phi(2,2) * phi(2,2)) ./D2;
 
    H12 = (phi(1,1) * phi(2,1)) ./D1 + (phi(1,2) * phi(2,2)) ./D2;
    H21 = H12;
 
figure 
semilogy(f,abs(H11), f, abs(H12), f, abs(H22), f,abs(H11V), f, abs(H12V), f, abs(H22V))
grid on
grid minor
legend ('H11', 'H12', 'H22','H11V', 'H12V', 'H22V')
title('H')
xlabel('Frequency [Hz]')
ylabel('H(w)')
 
%%%%%%%% Como visto nos gr�ficos, os dois m�todos s�o equivalentes
 
% subplot(2,1,1),semilogy(f,abs(H11))
% grid on
% title('Figure 1(a) H11')
% xlabel('Frequency [Hz]')
% ylabel('H_{pq}(w)')
% subplot(2,1,2),plot(f,180/pi*angle(H11))
% grid on
% 
% figure
% subplot(2,1,1),semilogy(f,abs(H22))
% grid on
% title('Figure 1(a) H22')
% xlabel('Frequency [Hz]')
% ylabel('H_{pq}(w)')
% subplot(2,1,2),plot(f,180/pi*angle(H22))
% grid on
% 
% figure
% subplot(2,1,1),semilogy(f,abs(H12))
% grid on
% title('Figure 1(a) H12')
% xlabel('Frequency [Hz]')
% ylabel('H_{pq}(w)')
% subplot(2,1,2),plot(f,180/pi*angle(H12))
% grid on
 
 
% H11_1 = N1(1,1) ./D1;
% H11_2 = N2(1,1)./D2;
% H22_1 = N1(2,2) ./D1;
% H22_2 = N2(2,2)./D2;
% subplot(2,1,1),semilogy(f,abs(H11_1),f,abs(H11_2),f,abs(H11))
% subplot(2,1,1),semilogy(f,abs(H22_1),f,abs(H22_2),f,abs(H22))
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parte B
 
Db1 = w.^2./D1;
Db2 = w.^2./D2;
 
%%%%%%%%%% TRr = (phi_1*phi_1' * [M1 M2]' * w^2)/D1 + (phi_2*phi_2' * [M1 M2]' * w^2)/D2
H11b = (phi(1,1) * phi(1,1) * M1 + phi(1,1) * phi(2,1) * M2) .* Db1 + (phi(1,2) * phi(1,2) * M1 + phi(1,2) * phi(2,2) * M2) .* Db2;
H22b = (phi(1,1) * phi(2,1) * M1 + phi(2,1) * phi(2,1) * M2) .* Db1 + (phi(1,2) * phi(2,2) * M1 + phi(2,2) * phi(2,2) * M2) .* Db2;
 
%%%%%%%%% TRr = [H] * [M1 M2]' * w^2
H11b_v3 = (H11 * M1 + H12 *M2) .* w.^2;
H22b_v3 = (H21 * M1 + H22 *M2) .* w.^2;
 
%%%%%%%% 
phim = phi * phi' * [M1 M2]';
H11b_v2 = phim(1) .* Db1 + phim(1) .* Db2;
H22b_v2 = phim(2) .* Db1 + phim(2) .* Db2;
 
 
figure 
semilogy(f,abs(H11b),'b',f,abs(H22b),'r',f,abs(H11b_v3),'g-.',f,abs(H22b_v3),'k--')
grid on
grid minor
legend ('H11','H22','H11 v2','H22 v2','H11 v3','H22 v3')
title('H')
xlabel('Frequency [Hz]')
ylabel('H(w)')

figure
plot(f,180/pi*angle(H22b))
% phi2 = phi * phi';
% phim = phi2 * [M1 M2]';
% Db1 = w.^2./D1;
% Db2 = w.^2./D2;
% 
% % phim = phi * phi' * [M1 M2]' * w.^2;
% 
% H1b = 1 + phim(1)./Db1 + phim(1)./Db2;
% H2b = 1 + phim(2)./Db1 + phim(2)./Db2;
% 
% H11b = H11 * M1 .* w.^2 + 1;
% % H12b = H12 * mrd(2) .* w.^2;
% % H21b = H21 * mrd(1) .* w.^2;
% H22b = H22 * M2 .* w.^2 + 1;
 
% H11b_v1 = N1(1,1) * M1 .* Db1 + N2(1,1) *M1 .* Db2;
% H22b_v1 = N1(2,2) * M2 .* Db1 + N2(2,2) *M2 .* Db2;
 
% phi1 = phi * phi';
% %phim = phi2 * [M1 M2]';
% %Db1 = w.^2./D1;
% %Db2 = w.^2./D2;
% 
% % phim = phi * phi' * [M1 M2]' * w.^2;
% 
% H1a = phi1(1,1)./D1 + phi1(1,1)./D2;
% H2a = phi1(2,2)./D1 + phi1(2,2)./D2;
 
% figure
% subplot(2,1,1),semilogy(f,abs(H12b))
% grid on
% title('Figure 1(b) H12')
% xlabel('Frequency [Hz]')
% ylabel('H_{pq}(w)')
% subplot(2,1,2),plot(f,180/pi*angle(H12b))
% grid on
% 
% figure
% subplot(2,1,1),semilogy(f,abs(H21b))
% grid on
% title('Figure 1(b) H21')
% xlabel('Frequency [Hz]')
% ylabel('H_{pq}(w)')
% subplot(2,1,2),plot(f,180/pi*angle(H21b))
% grid on
 
% figure
% subplot(2,1,1),semilogy(f,abs(H1b))
% grid on
% title('Figure 1(b) H11')
% xlabel('Frequency [Hz]')
% ylabel('H_{pq}(w)')
% subplot(2,1,2),plot(f,180/pi*angle(H1b))
% grid on
% 
% figure
% subplot(2,1,1),semilogy(f,abs(H2b))
% grid on
% title('Figure 1(b) H22')
% xlabel('Frequency [Hz]')
% ylabel('H_{pq}(w)')
% subplot(2,1,2),plot(f,180/pi*angle(H2b))
% grid on
% 
% 
% figure 
% semilogy(f,abs(H1b), f, abs(H2b),f,abs(H11b),f,abs(H22b))
% grid on
% grid minor
% legend ('H11', 'H22','H11','H22')
% title('H')
% xlabel('Frequency [Hz]')