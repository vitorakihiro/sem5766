% C�digo matlab desenvolvido pelo aluno Vitor Akihiro Hisano Higuti
% como parte do projeto 2 de SEM5766 turma 2016

numH=16;
DATA_SET = 2;

run('t_SEM766_useData2')
addpath('Functions')

freq = H1_2_cell_shaker{1,1}(:,1);
f_vec = freq;

%% Euler-Bernoulli
E = 191e9;                  % M�dulo de Young [N/m]
rho = 8000;                 % Densidade do aco comum [kg/m^3]
L = 1;                      % Comprimento da barra [m]
h = 9.55e-3;                % Altura da barra [m] 
b = 25.3e-3;                % Largura da barra [m]
A = h*b;                    % Area da secao transversal
I = b*h^3/12;
mi = rho*A;                  % Massa por unidade de comprimento

beta(1) = 1.50562*pi/L;
beta(2) = 2.49975*pi/L;
beta(3) = 3.50001*pi/L;
beta(4) = 4.50000*pi/L;
beta(5) = 5.5*pi/L;
beta(6) = 6.6*pi/L;

wn_EulerBernoulli = beta.^2*sqrt(E*I/mi);
fn_EulerBernoulli = wn_EulerBernoulli/(2*pi);

%%  Sistema de vibracao continua de 4a ordem
%   Secao 3.7 e 7.4.4 do livro Vibration Testing

% N�mero de modos
N = 10;
% Autovalores ajustados
% Newton Raphson with 10 iterations
lambda = [1e-3; 
    4.730040744862704; 
    7.853204624095838; 
    10.995607838001671; 
    14.137165491257464;
    17.278759657399480;
    20.420352245626060;
    23.561944902040455;
    26.703537555508188;
    29.845130209103473];
for i = length(lambda)+1:N       % aproximacao
    lambda(i) = pi*(i-0.5);
end

% Pontos de leitura do sensor (saida)
outLoc =[0.031, 0.356, 0.503, 0.98; 
    -0.005, 0.347, 0.503, 0.98;
    0.011, 0.353, 0.54, 0.98;
    0.001, 0.3465, 0.494, 1]';

% Pontos de aplicacao de excitacao (entrada)
inLoc = [0.008, 0.361, 0.507, 0.975;
    0.002, 0.352, 0.5004, 0.99;
    0.008, 0.352, 0.5, 0.985;
    0.008, 0.344, 0.492, 0.97];
 
w = 2*pi*f_vec;

% Matrizes modais
mp = eye(N);
kp = eye(N);
recalc = 0;
if exist ('kp.mat','file'), load('kp'); else recalc = 1; end
if exist ('mp.mat','file'), load('mp'); else recalc = 1; end

% Calculo dos elementos das matrizes modais, caso necessario
for i = 1:length(mp)
   if(recalc), mp(i,i) = fcnMp(lambda(i),L);end
   if(recalc), kp(i,i) = fcnKp(lambda(i),L);end
   fn_modeloModal(i)= sqrt(kp(i,i)/mp(i,i))/2/pi;
   fn_eulerBernoulli(i) = lambda(i)^2*sqrt(E*I/mi/L)/2/pi;
end

% Amortecimento proporcional
alpha = 1e-6;
cp = alpha*kp;

H = cell(4,4);
l = 1;
for j = 1:4
    for i = 1:4
        sum0 = 0;
        for p = 1:length(lambda)
            den = (kp(p,p)-mp(p,p)*w.^2 + sqrt(-1)*cp(p,p)*w);
            sum0 = sum0 + fcnPthModalShape( lambda(p), L, outLoc(l) )...
                *fcnPthModalShape( lambda(p), L, inLoc(l))./den;
        end        
        H{i,j} = -w.^2.*sum0;
    figure(1)
        subplot(4,4,l)        
            semilogy(freqS,abs(H1_2_cell_shaker{1,k_shaker(l)}(:,2)),'LineWidth',2)
        hold on
        semilogy(f_vec,abs(H{i,j}),'r')
        
    figure(2)
        subplot(4,4,l)  
        plot(freqS,imag(H1_2_cell_shaker{1,k_shaker(l)}(:,2))/...
            max(abs(imag(H1_2_cell_shaker{1,k_shaker(l)}(:,2)))),'LineWidth',2)
        hold on
        plot(f_vec,imag(H{i,j})/max(abs(imag(H{i,j}))),'ro-')
        l = l + 1;
    end
end

%% Ewins-Gleeson
    fig_dir = 'Figs/';    
    subtitle='';
    
    f_n_cell{1,1} = [1e-8 47.5 132.5 258.8 428.8 640.6 894.4];
    f_response_cell{1,1} = [ 15.63 41.25 75.63 120.6 180 245 323.1 413.1 516.3 625.6 743.1 875.6 993.8];
    f_n_cell{1,2} = [1e-8 49.38 135.6 266.3 439.4 654.4 915.6];
    f_response_cell{1,2} = [6.875 76.88 262.5 544.4 928.8 177.5 400.6 486.9 609.4];
    f_n_cell{1,3} = [0.625 48.13 133.8 260 433.1 641.3 901.9];
    f_response_cell{1,3}=[38.13 61.88 136.9 138.1 228.1 303.1 438.1  615.6 698.8 921.9];
    f_n_cell{1,4} = [1e-8 48.13 131.9 256.9 425.6 636.3 887.5];
    f_response_cell{1,4}=[10.63 83.13 186.3 333.1 515.6 749.4 246.3 270 415 436.3 874.4 900];
    f_n_cell{1,5} = [1e-8 48.13 133.8 262.5 430 643.1 901.9];
    f_response_cell{1,5}=[10.63 75 261.3 533.1 923.8 40 60.63 111.3 176.3 400 478.8 597.5 913.1 940.6];
    f_n_cell{1,6} = [1e-8 50 135 267.5 435 648.8 916.3];
    f_response_cell{1,6}=[39.38 102.5 266.3 358.8 590 914.4 15 80 198.8 327.5 507.5 780.6];
    f_n_cell{1,7} = [1e-8 48.75 135.6 264.4 437.5 645.6 916.3];
    f_response_cell{1,7}=[35.63 136.3 276.3 440 891.3 914.4 23.13 85 216.3 364.4 586.3 760.6 ];
    f_n_cell{1,8} = [1e-8 48.13 132.5 261.9 430 640 899.4];
    f_response_cell{1,8}=[31.88 78.13 219.4 280 361.9 515 852.5 905.6 912.5 963.8 ];
    f_n_cell{1,9} = [1e-8 48.13 133.8 259.4 433.1 640 902.5];
    f_response_cell{1,9}=[8.125 136.3 439.4 915 3.125 37.5 65.53 228.8 298.1 590.6 669.4 829.4 893.1  ];
    f_n_cell{1,10} = [1e-8 49.38 135 265 436.9 643.6 916.3];
    f_response_cell{1,10}=[35.63 136.3 274.4 439.4 883.8 914.4 24.38 91.25 208.1 366.3 590 761.3 894.4];
    f_n_cell{1,11} = [1e-8 49.38 261.9 645.6 ];%441.3 916.3];
    f_response_cell{1,11}=[31.25 195 543.8 21.88 121.9 388.8 810.6 ];%439.4 441.9 913.8 917.5];
    f_n_cell{1,12} = [1e-8 48.75 133.8 260 431.9 638.1 898.8];
    f_response_cell{1,12}=[137.5 445.6 925.6 3.125 40.63 58.75 130.6 218.1 279.4 424.4 590.6 680 888.1 914.4 951.9];
    f_n_cell{1,13} = [1e-8 48.13 130.6 257.5 425 635.6 885.6];
    f_response_cell{1,13}=[3.75 41.25 126.3 250 416.9 625.6 873.1];
    f_n_cell{1,14} = [1e-8 48.13 132.5 261.3 429.4 640.6 899.4];
    f_response_cell{1,14}=[1.875 52.5 148.1 270 438.1 655 903.1 ];
    f_n_cell{1,15} = [1e-8 48.75 133.1 259.4 431.9 639.4 898.8];
    f_response_cell{1,15}=[136.9 443.1 922.5 36.88 130.6 229.4 425.6 598.8 891.9 ];
    f_n_cell{1,16} = [1e-8 48.13 131.3 258.1 427.5 638.8 891.3];
    f_response_cell{1,16}=[34.38 111.3 232.5 396.3 603.8 853.1 18.13 81.88 184.4 328.8 508.8 731.3];
    
    f_n_cell{2,1} = [0.625 49.38 135.6  256.6 438.1 653.1 913.1];
    f_response_cell{2,1} = [ 36.25 113.1 235 401.9 611.3 865 25.63 86.88 195.6 335.6 529.4 758.8 964.4 ];
    f_n_cell{2,2} = [0.625 48.75 133.1 261.9 429.4 643.8 901.3];
    f_response_cell{2,2} = [14.38 75 262.5 545 800.6  928.8 3.75 39.38 59.38 110];
    f_n_cell{2,3} = [0.625 48.13 133.8 260 432.5 641.3 901.3];
    f_response_cell{2,3}=[1.25 8.75 100 135.6 439.4 913.1 216.3 355.6 586.9 841.9 ];    
    f_n_cell{2,4} = [0.625 49.38 135.6 265 438.1 651.9 911.3];    
    f_response_cell{2,4}=[16.25 81.25 200.6 338.8 552.5 770.6 ...
        41.88 126.3 256.3 428.1];
    f_n_cell{2,5} = [0.625 49.38 135.6 266.3 438.8 653.1 913.8];
    f_response_cell{2,5}=[6.25 70.63 261.9 542.5 927.5 ...
        40.63 111.9 259.4 390.6 783.1];    
    f_n_cell{2,6} = [0.625 49.38 136.3 267.5 440 655 918.8 ];
%     f_response_cell{2,6}=[39.38 102.5 266.3 358.8 590 914.4 15 80 198.8 327.5 507.5 780.6];
    f_response_cell{2,6}=[38.75 101.9 266.9 358.8 588.8 916.9 ...
        43.75 75 115.6 201.3 625.6 ];    
    f_n_cell{2,7} = [1e-4 49.38 136.3 266.9 440 655 ];
    f_response_cell{2,7}=[32.5 136.9 273.8 441.9 883.8 ...
        43.13 270.6 516.3 781.3 880.6 891.3 916.3 959.4 ];    
    f_n_cell{2,8} = [0.625 49.38 135.6 265.6 438.8 653.1 913.8 ];
    f_response_cell{2,8}=[28.75 275.6 898.1 80.63 210.6 537.5 ...
        40 128.8 760 945.6];    
    f_n_cell{2,9} = [1e-4 49.38 135.6 265.6 439.4 653.1 917.5];
    f_response_cell{2,9}=[8.75 100 136.9 386.3 441.3 821.3 921.9 ...
        37.5 133.8 911.3    ];    
    f_n_cell{2,10} = [0.625 49.38 136.3  266.9 440.6  655 ];%918.8];
    f_response_cell{2,10}=[32.5 136.9 273.8 440 878.8 916.3 ...
        26.25 138.8 276.3 444.4 771.3 895.6 920   ];    
    f_n_cell{2,11} = [0.625 49.38 266.3 654.4];%441.3 916.3];
    f_response_cell{2,11}=[33.13 194.4 541.3 ...
        40 233.8 621.9];    
    f_n_cell{2,12} = [0.625 49.38 135.6 265.6 438.8 653.1 914.4];
    f_response_cell{2,12}=[101.3 136.9 366.9 443.8 820.6 926.9 ...
        41.88 134.4 251.3 435.6 630 906.9 ];    
    f_n_cell{2,13} = [0.625 49.38 135.6 265 437.5  651.9 911.3];
    f_response_cell{2,13}=[26.88 88.75 191.9 339.4 538.8 773.1 ...
        41.88 126.3];    
    f_n_cell{2,14} = [0.625 49.38 135.6 266.3 438.8 653.8 915.6];
    f_response_cell{2,14}=[30.63 77.5 208.1 273.8 536.9 900 964.4 ...
        261.3 406.3 634.4];    
    f_n_cell{2,15} = [0.625 49.38 135.6  265.6 439.4 653.1 915.6];
    f_response_cell{2,15}=[90.63 136.9 363.8 441.3 796.3 919.4 ...
        60.63 300.6 693.1 ];
    f_n_cell{2,16} = [0.625 49.38 135.6 265 438.1 651.9 911.9];
    f_response_cell{2,16}=[35.63 112.5 233.8 398.8 608.1 860 ...
        15.63 88.13];   
    
    A_JK = zeros(7,16);
    l_in = [1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 ];
    l_out = [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4];
    H_EG = cell(4,4);
    for i =1:16
        clearvars A_jk eta_jk
        f_n = f_n_cell{DATA_SET,i};
        f_response = f_response_cell{DATA_SET,i};
        FRF_vec = H1_2_cell{1,k(i)}(:,2);

        % Compare the original and the identified FRFs for each configuration
        % of sensor and actuator.
        % Do not forget to pass the frequencies in rad/s
        [A_jk, eta_jk] = fcnEwinsGleeson(f_vec*2*pi, FRF_vec, f_n*2*pi, f_response*2*pi); 
        Ajk{1,i} = A_jk;
        A_JK(1:length(A_jk),i) = A_jk;
        reconstructedH = fcnEvalFrfEwinsGleeson(f_vec, A_jk, f_n, eta_jk);
        reconstructedH(abs(reconstructedH)>1e5)=1e3;
        H_EG{l_out(i),l_in(i)} = reconstructedH;
    end
            
    % Valor medio para as frequencias naturais experimentais
    A = zeros(size(f_n_cell,2), length(f_n_cell{1,1}) );
    A_hammer = zeros(size(f_n_cell,2), length(f_n_cell{1,1}) );
    for i  =1:16
        if(i~=11), 
            A(i,:) = f_n_cell{1,i}(1,:); 
            A_hammer(i,1:length(f_n_cell{2,i})) = f_n_cell{2,i}(1,:);
        end
    end
    A(11,:) = [];       % em H{3,3} so tem 4 fn 
    A_hammer(11,:) = [];       % em H{3,3} so tem 4 fn    
    fn_exp= mean(A);
    for i = 1:size(A_hammer,2)
        fn_exp_hammer(i) = sum(A_hammer(:,i))/nnz(A_hammer(:,i));
    end
    
    disp('Diferenca percentual entre experimental do shaker e do hammer')
    fcnPercentualDiff(fn_exp, fn_exp_hammer)
    disp('Diferenca percentual entre experimental do shaker e modelo Euler Bernoulli')
    fcnPercentualDiff(fn_exp, fn_eulerBernoulli)
    disp('Diferenca percentual entre experimental do shaker e modelo modal')
    fcnPercentualDiff(fn_exp, fn_modeloModal)
    disp('Diferenca percentual entre experimental do hammer e modelo Euler Bernoulli')
    fcnPercentualDiff(fn_exp_hammer, fn_eulerBernoulli)
    disp('Diferenca percentual entre experimental do hammer e modelo modal')
    fcnPercentualDiff(fn_exp_hammer, fn_modeloModal)
         
inputLoc = mean(inLoc);

for r = 1: 7%length(lambda)
    for j = 1:length(inputLoc)
        phi_model(r,j) = -fcnPthModalShape(lambda(r),L,inputLoc(j));
    end
end
 
figure(4)
l = 1;
for i = 1:16 
    subplot(4,4,l)
    semilogy(freqS, abs(H1_2_cell_shaker{1,k_shaker(i)}(:,2)), ...
        freqH, abs(H1_2_cell_hammer{1,k_hammer(i)}(:,2)) )
    l = l + 1;
end
 
% run('plotsModelo')
% run('plotsEwinsGleeson')

A_JK(6,11) = A_JK(4,11);
A_JK(4,11) = A_JK(3,11);
A_JK(3,11) = 0;

A_JK = real(A_JK);

aux1 = [1, 5, 9, 13]; % utilizando uma coluna
% aux1 = [1, 2, 3, 4]; % utilizando uma linha

% Obtencao dos autovetores a partir das constantes modais
for r = 1:7
    phiEg(r,1) = sqrt(A_JK(r,1));
    for j = 2:4 
        phiEg(r,j) = A_JK(r,aux1(j))/phiEg(r,1);
    end
end
% % phiEg(1,1) = sqrt(A_JK_used(1,1));
% % phiEg(1,2) = A_JK(1, 5)/phiEg(1,1);
% % phiEg(1,3) = A_JK(1, 9)/phiEg(1,1);
% % phiEg(1,4) = A_JK(1, 13)/phiEg(1,1);
% % 
% % phiEg(2,1) = sqrt(A_JK_used(2,1));%
% % phiEg(2,2) = A_JK(2,5)/phiEg(2,1);% r = 2, j = 1, k = 2
% % phiEg(2,3) = A_JK(2,9)/phiEg(2,1);% r = 2, j = 1, k = 3
% % phiEg(2,4) = A_JK(2,13)/phiEg(2,1);%r = 2, j = 1, k = 4
% % 
% % phiEg(3,1) = sqrt(A_JK_used(3,1));
% % phiEg(3,2) = A_JK(3,5)/phiEg(3,1); % r = 3; j = 1; k = 2 
% % phiEg(3,3) = A_JK(3,9)/phiEg(3,1); % r = 3; j = 1; k = 3 
% % phiEg(3,4) = A_JK(3,13)/phiEg(3,1); % r = 3; j = 1; k = 4  

figure
l = 1;
for  a = 1:4
    for b = 1:4
        subplot(4,4,l)
        plot(f_vec, imag(H_EG{a,b}),'r.-')
        l = l + 1;
    end
end

figModos = figure;
x = linspace(0,1,1e2);
l = 1; 
for p = 1:7
    for i = 1:length(x)
        up{p}(i) = fcnPthModalShape(lambda(p),L,x(i)); 
    end
    subplot(2,4,l)
    plot(x,-up{p}/max(abs(up{p})));
    hold on
    scatter(inputLoc, phi_model(p,:)/max(abs(phi_model(p,:))),'d',...
        'MarkerEdgeColor','r','LineWidth',2)
    scatter(inputLoc, phiEg(l,:)/max(abs(phiEg(l,:))), 'x',...
        'MarkerEdgeColor','k','LineWidth',2)
    
    mode_title = sprintf('Modo %i', p-1);
    title(mode_title)
    xlabel('x [m]');
    ylabel('y [m]')
    l = l+ 1;
end

fcnPlotMac(phiEg(1:7,:)', phi_model(1:7,:)','Modelo X Ewins-Gleeson')
fcnPlotMac(phiEg(2:7,:)', phi_model(2:7,:)','Modelo X Ewins-Gleeson sem f_n = 0 Hz')

figure(figModos)

% run('plotsModelo')